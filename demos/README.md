
Demo directory
==============

This directory is where demo application git repositories should be cloned. You
can find these on https://bitbucket.org/rvex by searching for
`plat-ml605-demo-`. At the time of writing, that would be:

https://bitbucket.org/rvex/profile/repositories?search=plat-ml605-demo-
