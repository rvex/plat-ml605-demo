
r-VEX ML605 demo platform
=========================

This platform supports the FreeRTOS and DOOM demos used at the TU Delft.


tl;dr
-----

If you just want to build the demo platform and call it a day, download the
demo(s) you want to run into the `demos` directory (they're separate GIT
repositories, refer to `demos/README.md`), and run `make all`. It's advisable
to monitor the process for build problems until it starts synthesizing; this
takes maybe 15 minutes for the first run. Synthesis itself can take two to
three hours easily, so you might want to just run this overnight.

The ISE environment can sometimes prevent other software from starting up
correctly due to incompatible `LD_PRELOAD`s. If this happens, run `make all`
without ISE's environment first to build the toolchain, then run it with its
environment to synthesize. There are some additional commands that the makefile
runs after synthesis, so *maybe* you'll have to run it again without the
environment. In my tests none of this was necessary, though.

When the build completes, get a CompactFlash cardreader, and run the following
commands on the CF card that comes with the board (replace `/dev/sdX` with your
card's device file):

    sudo dd if=bootloader/bin/bootloader of=/dev/sdX
    sudo mkfs.fat -F16 -s32 /dev/sdX1
    sudo mkfs.fat -F32 /dev/sdX2

After formatting, copy the files within `sysace/part1` to the root directory of
the first partition of the card, and the files within `sysace/part2` (if it
exists) to the root directory of the second partition.

If you can't be bothered connecting to the board's UART to select an
application, create a file named `autorun` in the root of partition 1 and write
the name of the demo in the file (don't write anything else, the bootloader
isn't very smart about this file). The name you write should be the name of the
ELF file without the `.*.elf` suffix, so to run FreeRTOS (`freertos.1.elf` and
`freertos.2.elf`) the contents of the file should just be `freertos`.

Put the card back in the board and make sure that the DIP switches are
configured as follows to boot from it:

    Boot from compactflash:
    .--------.
    | [  ##]6| FLASH_A23
    | [##  ]5| M2
    | [  ##]4| M1
    | [##  ]3| M0
    |o[##  ]2| CS_SEL
    |n[  ##]1| EXT CCLK
    |  <---  |
    '--------'
    .--------.
    | [##  ]4| SysAce Mode
    | [  ##]3| CFGAddr 2
    |o[  ##]2| CFGAddr 1
    |n[  ##]1| CFGAddr 0
    |  <---  |
    '--------'

Finally, power up the board and hope that the Christmas tree lights up
correctly.


Hardware requirements
---------------------

### DDR3 memory

The board requires either a 1 GiB or 512 MiB DDR3 SODIMM to be installed in the
board. Known good:

 - MT8JTF12864HZ-1G6G1 (1 GiB)
 - MT4JSF6464HY-1G1B1 (512 MiB) **TODO: TEST ME WITH REMAPPING**

If you have aliasing issues, you may need to remap the address bits in
`work/leon3mp.vhd`, or regenerate the MIG for your memory module.

### PS/2 and audio output

**WARNING: mess with your board at your own risk! You don't have to solder-mod
the board, but you'll still need to connect hacked electronics to it through the
supplied headers, which can also cause damage if you don't know what you're
doing.**

**If you don't care about the PS/2 or audio output, I recommend that you at
least disconnect the LCD display from the connector.**

This platform has two PS/2 ports and a mono audio output which, for which there
are no connectors on the board. You'll need to make a breakout board yourself if
you want to use them. The LCD header and LED output header are used as an
interface. The audio output is just a PWM signal at 2.5V; you should at the very
least use a capacitor for DC blocking, and it's probably a bad idea to apply an
inductive load. I'd suggest a circuit like this:

               +            ____              _
    LED pin ----|(----o----|____|----o---V   | |
              100nF   |  100R to 1k  |       | | Jack plug
             to 1uF  .-.             '-----^ |_|  socket
                     | | 1k to 100k           |
                     '-'                      |
                      |                       |
    LCD GND pin ------o-----------------------'

The capacitor blocks DC offset; if your capacitor is polarized, the positive
side should be on the LED pin side. The high-valued shunt resistor charges the
capacitor to the DC offset level. The low-valued series resistor prevents damage
to the pin in case of a short circuit.

PS/2 at 0x80000000, IRQ 6 (keyboard):

 - GND: J41 (LCD connector) pin 13
 - VCC: J41 (LCD connector) pin 14
 - CLK: J41 (LCD connector) pin 11
 - DAT: J41 (LCD connector) pin 10

PS/2 at 0x80000100, IRQ 7 (mouse):

 - GND: J41 (LCD connector) pin 13
 - VCC: J41 (LCD connector) pin 14
 - CLK: J41 (LCD connector) pin 9
 - DAT: J41 (LCD connector) pin 4

Audio output:

 - GND: J41 (LCD connector) pin 13
 - SIG: J62 (LED GPIO connector) pin 1


Building the toolchain
----------------------

The first step in the build process is to generate the requisite toolchains.

    plat-ml605-demo
     |- toolchain
     |   |- config-main*        - configuration directory for main r-VEX
     |   |   |- Makefile*       - toolchain builder Makefile for main r-VEX
     |   |   '- ...
     |   |- config-monitor*     - configuration directory for monitor r-VEX
     |   |   |- Makefile*       - toolchain builder Makefile for monitor r-VEX
     |   |   '- ...
     |   |- main*               - toolchain root for main r-VEX
     |   |   |- sourceme*       - source this script to compile for main r-VEX
     |   |   '- ...
     |   |- monitor*            - toolchain root for monitor r-VEX
     |   |   |- sourceme*       - source this script to compile for monitor r-VEX
     |   |   '- ...
     |   |- build-main*         - indicates when/that main has been built
     |   |- build-monitor*      - indicates when/that monitor has been built
     |   '- Makefile            - toolchain Makefile
     |- main-config.ini         - configuration for the main r-VEX core
     |- monitor-config.ini      - configuration for the monitor r-VEX core
     |- Makefile                - master Makefile
     '- ...

    * marks generated files/directories.

The toolchain directory contains a basic Makefile that chains into the tools
directory of the primary r-VEX repository to generate two toolchains: one for
the main r-VEX core, and one for the monitor. The configuration for these two
cores is stored in `main-config.ini` and `monitor-config.ini`. The toolchain
Makefile first creates the `config-main` and `config-monitor` subdirectories,
in which it configures the toolchain builder. It then proceeds to run their
Makefiles to perform the build, installing in the `main` and `monitor`
subdirectories.

Upon successful completion, it creates respectively a `build-main` and
`build-monitor` file, preventing the Makefile from chaining back into the
toolchain builder when the build should already be complete. The master Makefile
also checks for the existence of these files to detect whether the toolchains
have been compiled. The only files that are dependency-checked are the `*.ini`
files. Thus, if you make a modification to the toolchain and *want* to
recompile, you can either delete these two files, or better, use the toolchain
builder Makefiles in the `config-*` directories directly to only recompile what
you want and, if necessary, do selective cleaning.

Speaking of cleaning, the toolchain Makefile has two clean targets. The first,
`clean`, cleans only the files generated within the toolchain directory. The
second, `very-clean` also runs `clean` in the toolchain builder Makefile, which
removes all cached intermediate files within the tools directory of the main
r-VEX repository.


Synthesizing the platform
-------------------------

Once the toolchain has been built (and with it, the r-VEX VHDL sources have been
configured), we can synthesize the FPGA platform.

    plat-ml605-demo
     |- grlib
     |   |- grlib-gpl-1.3.7-b4144*          - grlib install directory
     |   |   '- ...
     |   |- grlib-gpl-1.3.7-b4144.tar.gz*   - grlib package from TU Delft FTP
     |   |- Makefile                        - grlib configuration Makefile
     |   '- patch-1.3.7-b4144.patch         - grlib patch file
     |- Makefile                            - master Makefile
     '- ...

The platform is based on the LEON3 demo platform of the GPL version of grlib,
build 4144. This version of grlib is mirrored on the TU Delft FTP. Because of
license incompatibilities, we redistribute grlib separate from the r-VEX, and
only provide minimal patch files along with it.

The `grlib-gpl-1.3.7-b4144` target of the grlib configuration Makefile performs
the download with `wget`, unpacks, and finally applies the supplied patch. The
master Makefile automatically does this when it needs grlib. If you want, the
Makefile can also regenerate the patch file for you after you make changes to
the grlib install directory using the `patch-1.3.7-b4144` target, and there is
of course a `clean` target that removes all generated files.

    plat-ml605-demo
     |- work*
     |   |- leon3mp.vhd*          - toplevel VHDL file
     |   |- leon3mp_mig39.ucf*    - master constraints file
     |   |- Makefile*             - synthesis Makefile
     |   '- ...
     |- work.patch                - project patch file
     |- Makefile                  - master Makefile
     '- ...

Like grlib itself, the synthesis project is built using a patch file that
replaces the LEON3 from the grlib demo project with the r-VEX, and also replaces
some of the peripherals with our own. This is done by the `work` target of the
master Makefile, outputting the patched project in the work directory. The
reverse operation can be done using the `update-patch` target, but keep in mind
that this first cleans the work directory using the synthesis Makefile. Finally,
the `very-clean` target removes the work directory entirely such that it can be
rebuilt from the patch file.

The eventual goal was to get rid of the grlib dependencies over time, but in the
end we decided to focus our efforts on the r-VEX core itself instead.

    plat-ml605-demo
     |- hdl
     |   |- main_rvex    - link to ../toolchain/main/rvex-elf32/hdl/rvex*
     |   |- mon_rvex     - link to ../toolchain/monitor/rvex-elf32/hdl/rvex*
     |   |- rvex         - link to ../toolchain/main/rvex-elf32/hdl/share*
     |   '- libs.txt
     '- ...

Since this platform uses grlib's Makefile, we need the r-VEX sources in a
directory format that grlib understands. That's what the hdl directory is for.
This directory contains symlinks to the VHDL sources of the two r-VEXs
(main_rvex and mon_rvex) and the shared peripheral sources (rvex). Note that the
directory names are also the names of the VHDL libraries that they are compiled
into. The shared sources must be compiled in the `rvex` library, whereas the
core sources each need their own library.

All this culminates in the `synth` target of the master Makefile. The primary
function of this target is to chain into the synthesis Makefile, which in turn
generates the necessary Xilinx IP blocks and then runs synthesis using
PlanAhead. You can also run the `ise` target to start ISE, but synthesis using
the GUI probably won't work right. Note that for all this to work, you need to
have the binaries of ISE 14.7 in your system path (that is, you need to source
its environment script).

    plat-ml605-demo
     |- versions
     |   |> cores                      - legacy, unused
     |   |> platforms                  - archives of all synthesis results
     |   |> tools                      - version system Python scripts
     |   |- latest-ml605-demo.bit*     - the newest bitfile
     |   |- latest-ml605-demo.tar.gz*  - symlink to the newest archive
     |   '- ...
     '- ...

Wrapped around this chaining is some version management (currently mostly copied
from the legacy system), which eventually makes a tar archive of the sources,
ISE logs, and generated bitstream in the `versions/platforms` directory. The
archive is tagged with a random-generated 7-character base64 string. This tag is
also synthesized into the platform tag register of the r-VEX cores, and is shown
in the output of `rvd ?`. This allows you to trace a bitstream back to its
sources and log files, assuming the bitfile was generated here and the version
directory has not been cleaned since.

If you run into timing issues, you can of course reduce the clock (the frequency
selection is at the bottom of `work/config.vhd`), but you can also try
resynthesizing with a different placer seed. This seed can be changed in the
master Makefile.


Configuring the board
---------------------

You can configure the board in two ways: using the compactflash card, or using
Impact and the USB-JTAG interface. For your convenience, the DIP switch
configurations for these two modes are shown below.

    Boot from compactflash:             Boot from JTAG:
    .--------.                          .--------.
    | [  ##]6| FLASH_A23                | [  ##]6| FLASH_A23
    | [##  ]5| M2                       | [##  ]5| M2
    | [  ##]4| M1                       | [  ##]4| M1
    | [##  ]3| M0                       | [##  ]3| M0
    |o[##  ]2| CS_SEL                   |o[##  ]2| CS_SEL
    |n[  ##]1| EXT CCLK                 |n[  ##]1| EXT CCLK
    |  <---  |                          |  <---  |
    '--------'                          '--------'
    .--------.                          .--------.
    | [##  ]4| SysAce Mode              | [  ##]4| SysAce Mode
    | [  ##]3| CFGAddr 2                | [  ##]3| CFGAddr 2
    |o[  ##]2| CFGAddr 1                |o[  ##]2| CFGAddr 1
    |n[  ##]1| CFGAddr 0                |n[  ##]1| CFGAddr 0
    |  <---  |                          |  <---  |
    '--------'                          '--------'

To configure the board using JTAG, you can use the `bitstream-program` target,
or you can run Impact manually. The original location of the bitstream is
`work/leon3mp.bit`, which is then copied to `bitstream.bit`, and archived in the
version directory.

To configure using the compactflash card, use the `bitstream-sysace` target of
the master Makefile. This will generate the following directory tree:

    plat-ml605-demo
     |- sysace*
     |   |- part1*                  - fist partition of the CF card (FAT16)
     |   |   |- ml605dem*
     |   |   |   |- rev0*
     |   |   |   |   '- rev0.ace*   - this file actually contains the bitstream
     |   |   |   '- xilinx.sys*
     |   |   '- xilinx.sys*
     |   '- ...
     |- Makefile                    - master Makefile
     '- ...

If you don't care about the bootloader and just want the bitstream to load
automatically, you can format the card yourself using either FAT12 or FAT16,
and then copy the contents of the `sysace/part1` to that partition. If you *do*
want to use the bootloader, continue reading. We'll handle formatting the card
later.


CompactFlash card bootloader
----------------------------

To allow the board to automatically boot into an application, a bootloader is
used. This bootloader resides in the following directory tree.

    plat-ml605-demo
     |- bootloader
     |   |- bin
     |   |   |- boot.elf*           - second-stage bootloader binary
     |   |   |- bootloader*         - the first (up to) 1MiB of the CF card
     |   |   |- Makefile            - bootloader Makefile
     |   |   |- mbr.elf*            - first-stage bootloader binary
     |   |   '- ...
     |   '- src
     |       |- boot.c              - second-stage bootloader main()
     |       |- elf.c               - second-stage bootloader ELF library
     |       |- elf.h                   sources
     |       |- mbr.S               - first-stage bootloader assembly file
     |       |- mbr.x               - first-stage bootloader linker script
     |       |- platform.c          - second-stage bootloader platform library
     |       '- platform.h              sources
     |- Makefile                    - master Makefile
     '- ...

When the board is configured, the main r-VEX immediately starts executing the
master boot record in 1x8 mode. The monitor r-VEX also starts executing, but its
instruction memory loads up all zeroed out, so it just executes NOPs (actually
multiply instructions using only $r0).

The bootloader consists of two stages. The first stage is a pure assembly copy
routine residing in the master boot record, that simply copies the free
reserved first 1 MiB of the card to to 0x18000000 and then jumps to 0x18000200.
The second stage consists of a rather heavyweight C program based on newlib
that sets up a filesystem, initializes peripherals and mounts them as device
files, and finally loads an ELF file and passes control.

To compile the bootloader, run:

    make bootloader

The bootloader makefile expects a CompactFlash card at least 512MiB in size. In
practice that means a 1GB card. You can reduce the size in the bootloader
makefile if you want, though.

To install the bootloader on the card, run the following command, replacing
`sdX` with your compactflash device file (without partition number):

    sudo dd if=bootloader of=/dev/sdX

This affects the master boot record, so if this is the first time you're
installing the bootloader or you've changed the card size in the makefile, you
also need to format the two partitions it creates:

    sudo mkfs.fat -F16 -s32 /dev/sdX1
    sudo mkfs.fat -F32 /dev/sdX2

After formatting, you should copy the files within `sysace/part1` of the
platform directory to the root directory of the first partition of the card,
and the files within `sysace/part2` (if it exists) to the root directory of the
second partition.

The first partition is used by the sysace controller IC to program the FPGA.
The bootloader will also look for ELF files here. It is mounted read-only by
the bootloader to prevent accidental corruption. The second partition is
intended to be used by the applications. It is mounted read/write.

The application to load is selected as follows:

 - If there is a value in `CR_SCRP3` (scratchpad 3), this value is interpreted
   as a string containing the program name. This can be utilized by the monitor
   core to request a reboot if it detects that the main core has crashed: it
   has to then reset the main core and subsequently write to the scratchpad
   using the debug bus.
 - If a file named `autorun` exists in the root directory of partition 1, its
   contents are interpreted as the program name. The bootloader will wait for
   UART input for 5 seconds before loading the program to let you override the
   automatic boot.
 - If any of the above loading methods fail, the loader runs a *very*
   rudimentary shell over the UART with a command to load a program by name,
   as well as a couple test commands.

An application consists of one or both of the following ELF files:

 - `<name>.1.elf`: the ELF file for the main core.
 - `<name>.2.elf`: the ELF file for the secondary core.

The main ELF file can be compiled in one of three ways:

 - newlib with start files (default): the application initializes the board
   from scratch. This gives the application the greatest flexibility with
   newlib.
 - newlib without start files (`-nostartfiles`): the program uses the
   bootloader's system calls. That is, it uses the bootloader filesystem and
   remaining heap space. This should make applications highly portable, since
   the bootloader basically becomes the operating system. `libfs` does *not*
   need to be linked against the application in this case, since `libfs` is
   pretty much just the OS layer.
 - bare metal (`-mruntime=bare`): the application can do whatever it wants, at
   the cost of having to do everything by itself.

The secondary ELF file should be compiled with the default configuration of the
monitor toolchain.

In all cases, the loading process is as follows:

 - Halt the secondary core.
 - Load/zero all `ALLOC` sections specified by the two ELF files.
 - If a `main` symbol exists in the main ELF file, use that as a start address.
   If that doesn't exist, just use address 0.
 - If a secondary ELF file was specified, the secondary core is reset and
   started up again.
 - If a primary ELF file was specified, call its start address, with `$r3`
   set to `argc` and `$r4` set to `argv`. If this call returns, call `exit()`
   with its return value.
 - If no primary ELF file was specified, call `exit(0)`.

The filesystem initialized by the bootloader is as follows:

    /
     |- dev
     |   |- input
     |   |   |- event1              - PS/2 port 1 events
     |   |   '- event2              - PS/2 port 2 events
     |   |- dsp                     - audio output (TODO)
     |   |- fb0                     - VGA/DVI framebuffer output (TODO)
     |   |- i2c1                    - DVI/Chrontel I2C interface
     |   |- i2c2                    - PMbus I2C interface
     |   |- sda                     - CF card block device
     |   |- sda1                    - partition 1 of the CF card
     |   |- sda2                    - partition 2 of the CF card
     |   '- ttyS0                   - debug UART (default stdin/stdout/stderr)
     |- part1                       - read-only mount point for partition 1
     |   '- ...
     '- part2                       - R/W mount point for partition 2
         '- ...

The device files are *somewhat* compatible with their Linux counterparts,
although most header files are named differently and lots of things are not
supported. For more information, refer to the libfs documentation in the
rvex-drivers repository.

The bootloader also provides a working `gettimeofday` syscall, which loads up
at midnight January 1st, 2018. It runs a bit slow or fast if the r-VEX clock
in MHz is not an integer (by default it's at 33 1/3 MHz), so don't rely on it
for anything that needs accuracy.


Bypassing the bootloader (debugging)
------------------------------------

If you want to bypass the bootloader and just load a program into the core
manually using the debug tools, you'll probably want to remove the following
line from `work/leon3mp.vhd`:

    irqi_l(0).rstvec <= X"40000";--X"000"

That will make the r-VEX start from address 0, like the generated debug tools
expect. But of course, you'd need to resynthesize for that. You can also use

    rvd -c all b
    rvd -c all rst
    rvd -c 0   w PC 0
    rvd -c all c

to force the PC to 0 using the debug interface.


Downloading and compiling demo applications
-------------------------------------------

The demo applications are bundled in separate repositories, matching the name
pattern `plat-ml605-demo-`. At the time of writing, you can get the list at

https://bitbucket.org/rvex/profile/repositories?search=plat-ml605-demo-

To add a demo application to the project, clone its repository in the demos
directory. To install an application, run `make` within the subrepo's root
directory and update the CompactFlash card from the `sysace/part*` directories.


Memory map
----------

This is the memory map as seen by the debug interface, the primary r-VEX core,
and the data access bus of the secondary r-VEX core. The instruction bus of the
secondary r-VEX can only access its local instruction memory, which is just
contiguously mirrored accross its entire address space.

As copypasted from `work/leon3mp.vhd` (run `make work` if it doesn't exist):

    .-----------.---------------.------------.-----.----------------------------------------------.
    | Periph.   | Address space | Bw. compat.| IRQ | Description                                  |
    |- - - - - -+- - - - - - - - `------. - -+- - -+- - - - - - - - - - - - - - - - - - - - - - - |
    | ahbmig    | 0x00000000..0x3FFFFFFF | * |     | Main memory                                  |
    | sysace    | 0x40000000..0x7FFFFFFF |   |     | System ACE interface (compact flash) #       |
    | apbps2    | 0x80000000..0x800000FF |   | 6   | Bottom PS/2 connector                        |
    | apbps2    | 0x80000100..0x800001FF |   | 7   | Top PS/2 connector                           |
    | irqmp     | 0x80000200..0x800002FF | * |     | Interrupt controller                         |
    | gptimer   | 0x80000300..0x800003FF | * | 8,9 | gettimeofday() or general purpose timers     |
    | gptimer   | 0x80000400..0x800004FF |   | 1   | System tick and audio samplerate timers      |
    | gpio      | 0x80000500..0x800005FF |   | 10  | Switches, buttons, LEDs                      |
    | svgactrl  | 0x80000600..0x800006FF | * |     | VGA controller                               |
    | i2cmst    | 0x80000700..0x800007FF |   | 11  | Chrontel DAC and DVI I2C bus                 |
    | i2cmst    | 0x80000800..0x800008FF |   | 12  | PMBus                                        |
    | i2cmst    | 0x80000900..0x800009FF |   | 13  | External I2C bus for Zebro                   |
    | ---       | 0x80000A00..0xCFFFFFFF |   |     | ---                                          |
    | rvex      | 0xD0000000..0xD0001FFF | * |     | Primary r-VEX core                           |
    | trace     | 0xD0002000..0xD0003FFF | * |     | Trace buffer for primary r-VEX core          |
    | ---       | 0xD0004000..0xD0FFFFFF |   |     | ---                                          |
    | dbguart   | 0xD1000000..0xD1000007 | * | 2   | Debug UART                                   |
    | ---       | 0xD1000008..0xD1FFFFFF |   |     | ---                                          |
    | audio     | 0xD2000000..0xD2000003 |   | 5   | Audio output buffer access                   |
    | ---       | 0xD2000004..0xD30FFFFF |   |     | ---                                          |
    | blockram  | 0xD3100000..0xD311FFFF |   |     | Instruction memory for secondary r-VEX core  |
    | ---       | 0xD3120000..0xD31FFFFF |   |     | ---                                          |
    | blockram  | 0xD3200000..0xD321FFFF |   |     | Data memory for secondary r-VEX core         |
    | ---       | 0xD3220000..0xD32FFFFF |   |     | ---                                          |
    | shadow    | 0xD3300000..0xD331FFFF |   |     | Write-only area affecting both IMEM and DMEM |
    | ---       | 0xD3320000..0xD3DFFFFF |   |     | ---                                          |
    | trace     | 0xD3E00000..0xD3E01FFF |   |     | Trace buffer for secondary r-VEX core        |
    | ---       | 0xD3E02000..0xD3EFFFFF |   |     | ---                                          |
    | rvex      | 0xD3F00000..0xD3F003FF |   |     | Secondary r-VEX core                         |
    | ---       | 0xD3F00400..0xFFFFFFFF |   |     | ---                                          |
    '-----------'------------------------'---'-----'----------------------------------------------'
    
    *: backwards compatible with old grlib platform.
    
    #: this region maps one-to-one to the compactflash card, except for writes
    to the last word, which serves as a control register. The main processor
    starts running at the start of this region, mapping to the master boot
    record of the card. The control register controls whether writes are
    allowed (off after reset) and which "bank" to access. Writes to this
    register (address 0x7FFFFFFC) must be:
    
    .-------.-------.-------.-------.-------.-------.-------.-------.
    |0:0:0:1|0:0:1:0|0:0:1:1|0:1:0:1|0:1:0:1|1:0:1:0|1:0:1:0|-:W:B:B|
    '-------'-------'-------'-------'-------'-------'-------'-------'
    0x  1       2       3       5       5       A       A       *
    
    where B is the bank and W is the active-high "writable" flag. Writes to
    the card when the "writable" flag is low or incorrect writes to the
    control register result in a bus fault. The bank mapping is as follows:
    
     Bank 0: 0x40000000..0x7FFFFFFB -> CF 0x00000000..0x3FFFFFFB
     Bank 1: 0x40000000..0x7FFFFFFB -> CF 0x20000000..0x5FFFFFFB
     Bank 2: 0x40000000..0x7FFFFFFB -> CF 0x40000000..0x7FFFFFFB
     Bank 3: 0x40000000..0x5FFFFFFF -> CF 0x60000000..0x7FFFFFFF
         and 0x60000000..0x7FFFFFFB -> CF 0x00000000..0x1FFFFFFB
    This allows for a 2 GiB card, where the first 1 GiB minus one word can be
    accessed without banking.

The "old grlib platform" refers to the ml605-grlig platform in the legacy
repository (rvex-rewrite, release 4.1 and 4.2).
