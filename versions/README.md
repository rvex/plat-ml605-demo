
Note: this folder is pretty much just copied from the legacy rvex-rewrite
repository, with some changes made to tools/paths.py for the new repo. But it
should probably be refactored a bit better in the future. In any case, its
purpose is to manage creation of platform tags and store the results of past
synthesis runs.
