
.NOTPARALLEL:
.SUFFIXES:

.PHONY: all
all:
	$(MAKE) toolchain
	test -e bitstream.bit || $(MAKE) synth
	$(MAKE) bitstream-sysace
	$(MAKE) bootloader
	$(MAKE) demo

# Cleans most intermediate files.
.PHONY: clean
clean: work-clean tool-clean boot-clean demo-clean

# Cleans more intermediate files. If you made changes in the work directory and
# haven't run update-patch yet, this will also remove your changes! Pretty much
# the only thing that this doesn't clean is the bitfile in the root directory.
.PHONY: very-clean
very-clean: clean work-very-clean tool-very-clean sysace-very-clean \
	grlib-very-clean

#------------------------------------------------------------------------------
# Recursion into the toolchain directory
#------------------------------------------------------------------------------

TOOLCHAIN = toolchain/build-main toolchain/build-monitor
TOOLCHAIN_DEPS = main-config.ini monitor-config.ini

.PHONY: toolchain
toolchain: $(TOOLCHAIN)

$(TOOLCHAIN): $(TOOLCHAIN_DEPS)
	$(MAKE) -C toolchain

.PHONY: tool-%
tool-%:
	$(MAKE) -C toolchain $*


#------------------------------------------------------------------------------
# GRLIB project management
#------------------------------------------------------------------------------

# Call the makefile in the grlib directory to download and patch grlib.
grlib/grlib-gpl-1.3.7-b4144:
	cd grlib && $(MAKE) grlib-gpl-1.3.7-b4144

# Cleans grlib downloaded content.
.PHONY: grlib-very-clean
grlib-very-clean:
	-$(MAKE) -C grlib clean

# Copies the base project from grlib into work and patches it.
work: grlib/grlib-gpl-1.3.7-b4144
	@if [ -d work ]; then \
		touch work; \
		echo "Touched work directory for make..."; \
	else \
		cp -r grlib/grlib-gpl-1.3.7-b4144/designs/leon3-xilinx-ml605 .; \
		mv ./leon3-xilinx-ml605 ./work; \
		cd work && patch -p1 < ../work.patch; \
		echo "Rebuilt work directory from patchfile..."; \
	fi

# Updates the patchfile based upon the differences between the grlib base
# project and the current contents of work. Kind of the inverse operation of
# the "work" target.
.PHONY: update-patch update-%.patch
update-patch: update-work.patch
update-%.patch: work-clean
	@cd work && $(MAKE) distclean
	diff -rupN --exclude="ram.srec" --exclude="ptag.vhd" \
		grlib/grlib-gpl-1.3.7-b4144/designs/leon3-xilinx-ml605/ work/ \
		> $(patsubst update-%,%,$@) ; true

# Cleans all grlib and rvex compilation intermediate files.
.PHONY: work-clean
work-clean:
	-cd work && $(MAKE) distclean scripts-clean migclean
	rm -f work/*_beh.prj
	rm -f work/xilinx.log
	rm -f work/timing.twr
	rm -rf work/archive
	rm -f synth.patch

# Removes the entire working directory; use with care (because the work dir
# might contain source files as well, which may not have been put in the patch
# file yet).
.PHONY: work-very-clean
work-very-clean: work-clean
	rm -Irf work


#------------------------------------------------------------------------------
# GRLIB makefile chaining/shorthands
#------------------------------------------------------------------------------

MAP_COST_TABLE = 1

ARCHIVE_TOOLS = versions/tools
ARCHIVE_DIR = work/archive

# Chain to the grlib makefile.
gr-%: work
	@cd work && $(MAKE) $(patsubst gr-%,%,$@)

# Runs synthesis. This also archives the core when it finishes generating.
.PHONY: synth
synth: work
	
	# Version management.
	$(MAKE) update-synth.patch
	$(ARCHIVE_TOOLS)/archive_platform_prepare.py $(ARCHIVE_DIR)
	rm -f synth.patch
	
	# Synthesis.
	cd work && $(MAKE) mig39 planahead MAP_COST_TABLE=$(MAP_COST_TABLE)
	
	# More version management.
	touch work/xilinx.log
	cat work/planahead/leon3-*/*.runs/synth_1/runme.log >> work/xilinx.log
	cat work/planahead/leon3-*/*.runs/impl_1/runme.log >> work/xilinx.log
	cat work/planahead/leon3-*/*.runs/impl_1/*.twr > work/timing.twr
	$(ARCHIVE_TOOLS)/archive_platform_complete.py $(ARCHIVE_DIR)
	
	# Copy the bitstream from the grlib project into the platform
	# directory.
	cp work/leon3mp.bit bitstream.bit

# Shorthand for launching ISE.
ise: work
	cd work && $(MAKE) \
		scripts-clean\
		migclean\
		mig39\
		ise-launch


#------------------------------------------------------------------------------
# FPGA bitstream programming
#------------------------------------------------------------------------------

.PHONY: program
bitstream-program: bitstream.bit
	echo "setMode -bs" > impact.cmd
	echo "setCable -port auto" >> impact.cmd
	echo "Identify -inferir" >> impact.cmd
	echo "identifyMPM" >> impact.cmd
	echo "assignFile -p 2 -file $<" >> impact.cmd
	echo "Program -p 2" >> impact.cmd
	echo "quit" >> impact.cmd
	impact -batch impact.cmd
	rm -f impact.cmd

.PHONY: bitstream-sysace
bitstream-sysace: sysace/part1/ml605dem/rev0/rev0.ace
sysace/part1/ml605dem/rev0/rev0.ace: bitstream.bit
	rm -rf sysace/part1-temp
	mkdir -p sysace/part1-temp
	echo 'setMode -acecf' > impact.cmd
	echo 'addConfigDevice -size "0" -name "ml605dem" -path "$(shell pwd)/sysace/part1-temp/"' >> impact.cmd
	echo 'addCollection -name "ml605dem"' >> impact.cmd
	echo 'addDesign -version 0 -name "rev0"' >> impact.cmd
	echo 'addDeviceChain -index 0' >> impact.cmd
	echo 'setAttribute -configdevice -attr size -value "generaic"' >> impact.cmd
	echo 'setAttribute -configdevice -attr reserveSize -value "0"' >> impact.cmd
	echo 'setAttribute -configdevice -attr name -value "XCCACE-AUTO"' >> impact.cmd
	echo 'addDevice -p 1 -file "$(shell pwd)/bitstream.bit"' >> impact.cmd
	echo 'generate -active ml605dem' >> impact.cmd
	echo "quit" >> impact.cmd
	impact -batch impact.cmd
	rm -f impact.cmd
	mkdir -p sysace/part1
	cp -rf -t sysace/part1 sysace/part1-temp/*
	rm -rf sysace/part1-temp

.PHONY: sysace-very-clean
sysace-very-clean:
	rm -rf sysace


#------------------------------------------------------------------------------
# Bootloader makefile chaining
#------------------------------------------------------------------------------

.PHONY: bootloader
bootloader:
	$(MAKE) -C bootloader/bin

.PHONY: boot-%
boot-%:
	$(MAKE) -C bootloader/bin $*


#------------------------------------------------------------------------------
# Demo application makefile chaining
#------------------------------------------------------------------------------

.PHONY: demo
demo:
	$(MAKE) -C demos all

.PHONY: demo-clean
demo-clean:
	-$(MAKE) -C demos clean

