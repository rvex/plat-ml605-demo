
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <input.h>
#include <sys/time.h>
#include <machine/rvex.h>
#include <sys/dirent.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <dsp.h>
#include <grsvga.h>

#include "elf.h"

typedef int (*main_t)(int argc, char *argv[]);
typedef void (*void_t)(void);

/**
 * Tries to load the given program. This only returns if loading fails.
 */
static void load(const char *progname) {
  
  fprintf(stderr, "Loading program \"%s\"...\n", progname);
  
  static const elf_mem_offs_t mem_offs = {};
  static const elf_mem_ill_t mem_ill_main[] = {
    { 0x18000000, 0xFFFFFFFF },
    { 0, 0 }
  };
  static const elf_mem_ill_t mem_ill_mon[] = {
    { 0x00000000, 0xD3100000 },
    { 0xD3300000, 0xFFFFFFFF },
    { 0, 0 }
  };
  
  elf_sym_req_t sym_req[] = {
    { "_start", -1 },
    { "main", -1 },
    { 0, 0 }
  };
  
  char fname[256];
  char *argv[2] = {fname, NULL};
  
  // Stop secondary core.
  CR_DCR_REL(0xD3F00000) = CR_DCR_E_MASK | CR_DCR_B_BIT;
  
  // Load secondary core.
  snprintf(fname, 256, "/part1/%s.2.elf", progname);
  int mon_ok = !elf_load(fname, &mem_offs, mem_ill_mon, 0);
  
  // Load primary core.
  snprintf(fname, 256, "/part1/%s.1.elf", progname);
  int main_ok = !elf_load(fname, &mem_offs, mem_ill_main, sym_req);
  
  // Fail if both main and monitor broke.
  if (!main_ok && !mon_ok) {
    fprintf(stderr, "Failed to load either ELF file!\n");
    return;
  }
  
  // Figure out the entry point for the primary core.
  unsigned long start;
  if (sym_req[0].addr != 0xFFFFFFFF) {
    start = sym_req[0].addr;
    fprintf(stderr, "Starting at 0x%08X (_start).\n", start);
  } else if (sym_req[1].addr != 0xFFFFFFFF) {
    start = sym_req[1].addr;
    fprintf(stderr, "Starting at 0x%08X (main).\n", start);
  } else {
    start = 0;
    fprintf(stderr, "Starting at 0x%08X (default).\n", start);
  }
  
  // Reset the secondary core if we loaded it.
  if (mon_ok) {
    fprintf(stderr, "Starting secondary core...\n");
    CREG_UINT32_RW(CR_GSR_REL_ADDR(0xD3F00000)) = CR_GSR_R_MASK;
  }
  
  // Pass control of the primary core to the application, or exit.
  fprintf(stderr, "Passing control to application... Have fun!\n--------\n");
  if (main_ok) {
    exit(((main_t)start)(1, argv));
  } else {
    exit(0);
  }
  
}

/**
 * Shows a directory listing of the specified directory.
 */
static int ls(const char *path) {
  
  int res;
  
  // Print header.
  fprintf(stderr, "Listing of %s:\n", path);
  
  // Open the directory.
  DIR *dirp = opendir(path);
  if (!dirp) {
    perror("opendir");
    return -1;
  }
  
  // Read entries.
  struct dirent de;
  while (1) {
    
    int pos = telldir(dirp);
    
    // Read the next entry.
    struct dirent *deres;
    res = readdir_r(dirp, &de, &deres);
    if (res) {
      errno = res;
      perror("readdir");
      closedir(dirp);
      return res;
    }
    
    // Check for EOF.
    if (!deres) break;
    
    // Print the entry.
    char perm[11];
    const char *color = "0";
    switch (deres->d_type) {
      case DT_DIR: perm[0] = 'd'; color = "1;34"; break;
      case DT_REG: perm[0] = '-'; break;
      case DT_BLK: perm[0] = 'b'; color = "1;33"; break;
      case DT_CHR: perm[0] = 'c'; color = "36"; break;
      default: perm[0] = '?'; color = "1;33"; break;
    }
    perm[1] = (deres->d_mode & 0400) ? 'r' : '-';
    perm[2] = (deres->d_mode & 0200) ? 'w' : '-';
    perm[3] = (deres->d_mode & 0100) ? 'x' : '-';
    perm[4] = (deres->d_mode & 0040) ? 'r' : '-';
    perm[5] = (deres->d_mode & 0020) ? 'w' : '-';
    perm[6] = (deres->d_mode & 0010) ? 'x' : '-';
    perm[7] = (deres->d_mode & 0004) ? 'r' : '-';
    perm[8] = (deres->d_mode & 0002) ? 'w' : '-';
    perm[9] = (deres->d_mode & 0001) ? 'x' : '-';
    perm[10] = 0;
    
    fprintf(stderr,
            "  %s %2d root root %8lu \033[%sm%-20s\033[0m 0x%08lX %d\n", 
            perm,
            deres->d_nlinks,
            (unsigned long)deres->d_size,
            color,
            deres->d_name,
            deres->d_ino,
            pos);
    
  }
  
  // Close the directory;
  res = closedir(dirp);
  if (res) {
    errno = res;
    perror("closedir");
    return res;
  }
  
  // Success.
  return 0;
}

/**
 * Tests PS/2 keyboard/mouse input.
 */
void ps2(void) {
  
  fprintf(stderr, "Showing PS/2 input! Press ESC on the keyboard or enter\nin the UART to exit back to bootloader.\n");
  
  int fd1 = open("/dev/input/event1", O_RDONLY | O_NONBLOCK, 0);
  if (fd1 < 0) perror("open event1");
  
  int fd2 = open("/dev/input/event2", O_RDONLY | O_NONBLOCK, 0);
  if (fd2 < 0) perror("open event2");
  
  int fd3 = open("/dev/ttyS0", O_RDONLY | O_NONBLOCK, 0);
  if (fd3 < 0) perror("open uart");
  
  while (1) {
    
    // Read from descriptor 1.
    struct input_event ev;
    if (read(fd1, &ev, sizeof(ev)) == sizeof(ev)) {
      fprintf(stderr, "%d.%06ds: ", ev.time.tv_sec, ev.time.tv_usec);
      switch (ev.type) {
        case EV_KEY:
          fprintf(stderr, "key %d ", ev.code);
          switch (ev.value) {
            case 0: fprintf(stderr, "up\n"); break;
            case 1: fprintf(stderr, "down\n"); break;
            case 2: fprintf(stderr, "keymatic\n"); break;
            default: fprintf(stderr, "???\n"); break;
          }
          if (!ev.value && (ev.code == KEY_ESC)) goto end;
          break;
        case EV_REL:
          switch (ev.code) {
            case REL_X: fprintf(stderr, "dx %d\n", ev.value); break;
            case REL_Y: fprintf(stderr, "dy %d\n", ev.value); break;
            default: fprintf(stderr, "unknown EV_REL %d = %d\n", ev.code, ev.value); break;
          }
          break;
        case EV_CHR:
          fprintf(stderr, "char '%c'\n", (char)ev.code); break;
          break;
        default:
          fprintf(stderr, "unknown event %d %d %d\n", ev.type, ev.code, ev.value);
          break;
      }
    }
    
    // Stop if we receive any UART input.
    char c;
    if (read(fd3, &c, 1) == 1 && c == '\n') goto end;
    
    // Swap descriptors so we check the other one next.
    int x = fd1;
    fd1 = fd2;
    fd2 = x;
    
  }
  
end:
  if (fd1 >= 0) close(fd1);
  if (fd2 >= 0) close(fd2);
  if (fd3 >= 0) close(fd3);
  
}

/**
 * Tests the audio output by writing a 1kHz triangle wave for one second.
 */
void audio(void) {
  
  fprintf(stderr, "Testing audio output...\n");
  
  int fd = open("/dev/dsp", O_WRONLY | O_NONBLOCK, 0);
  if (fd < 0) {
    perror("Failed to open /dev/dsp");
    return;
  }
  
  int val;
  
  // Set samplerate.
  val = 44100;
  fprintf(stderr, "Setting samplerate to %d... ", val);
  if (ioctl(fd, SNDCTL_DSP_SPEED, &val) < 0) {
    perror("ioctl failed");
  } else {
    fprintf(stderr, "actual is %d\n", val);
  }
  
  // Set samplerate.
  val = AFMT_S16_BE;
  fprintf(stderr, "Setting sample format to %d... ", val);
  if (ioctl(fd, SNDCTL_DSP_SETFMT, &val) < 0) {
    perror("ioctl failed");
  } else {
    fprintf(stderr, "actual is %d\n", val);
  }
  
  // Set mono.
  val = 0;
  fprintf(stderr, "Setting stereo to %d... ", val);
  if (ioctl(fd, SNDCTL_DSP_STEREO, &val) < 0) {
    perror("ioctl failed");
  } else {
    fprintf(stderr, "actual is %d\n", val);
  }
  
  // Sample buffer.
  short buf[1024];
  
  int t = 0;
  while (t < 44100) {
    
    // Synthesize the waveform. I tried a sine first, but it was too slow :(
    int i;
    short *bufptr = buf;
    for (i=0; t<44100 && i<1024; t++, i++, bufptr++) {
      
      float vol, phase, f;
      
      vol = 1.0f - t * (1.0f / 44100.0f);
      /*phase = t * ((1.0f / 44100.0f) * 1000.0f * 2.0f * 3.14159265f);
      lchf = sinf(phase);*/
      phase = t * ((1.0f / 44100.0f) * 1000.0f);
      phase -= (float)(int)phase;
      phase = phase * 4.0f - 1.0f;
      if (phase > 1.0f) phase = 2.0f - phase;
      f = phase * vol;
      *bufptr = (short)(f * 20000.0f);
      
    }
    
    // Write to the buffer.
    const void *bufptr2 = (const void*)buf;
    int remain = i*2;
    while (remain) {
      int count = write(fd, bufptr2, remain);
      if ((count < 0) && (errno != EAGAIN)) {
        perror("Write failed");
        close(fd);
        return;
      } else if (count > 0) {
        remain -= count;
        bufptr2 += count;
      }
    }
    
  }
  
  fprintf(stderr, "Done!\n");
  close(fd);
}

/**
 * Puts a couple pixels on the screen with a seek and a write. For palette
 * modes, put the palette index in r. For other modes, just use values
 * between 0 and 255 for the channels.
 */
int wpix(int fd, struct fb_var_screeninfo *info, int x, int y, int count, unsigned char r, unsigned char g, unsigned char b) {
  
  // Figure out the offset of the pixel within the framebuffer.
  int offs = (info->bits_per_pixel >> 3) * (x + y * info->xres);
  
  // Seek to the first pixel.
  if (lseek(fd, offs, SEEK_SET) < 0) {
    perror("Failed to seek to pixel");
    close(fd);
    return -1;
  }
  
  // Figure out the color.
  unsigned int col;
  if (info->bits_per_pixel <= 8) {
    col = r;
  } else {
    col  = (r >> (8 - info->red.length))   << info->red.offset;
    col |= (g >> (8 - info->green.length)) << info->green.offset;
    col |= (b >> (8 - info->blue.length))  << info->blue.offset;
  }
  
  // Allocate a buffer for the pixels.
  int size = count * (info->bits_per_pixel >> 3);
  void *buf = malloc(size);
  if (!buf) {
    perror("Failed to create pixel buffer");
    close(fd);
    return -1;
  }
  
  // Replicate the color along the buffer.
  int i;
  void *bufptr = buf;
  switch (info->bits_per_pixel) {
    case 16: while (count--) { *((unsigned short*)bufptr) = col; bufptr += 2; } break;
    case 32: while (count--) { *((unsigned int*)  bufptr) = col; bufptr += 4; } break;
    default: while (count--) { *((unsigned char*) bufptr) = col; bufptr += 1; } break;
  }
  
  // Write the pixels.
  if (write(fd, buf, size) != size) {
    perror("Failed to write pixels");
    free(buf);
    close(fd);
    return -1;
  }
  
  // Done.
  free(buf);
  return 0;
}

/**
 * Tests the video output.
 */
void video(char *arg) {
  int i;
  
  fprintf(stderr, "Testing video output!\n");
  
  // Figure out mode to request.
  int xres = 640;
  int bpp = 16;
  sscanf(arg, "%d %d", &xres, &bpp);
  fprintf(stderr, "Requesting mode with xres >= %d and bpp >= %d\n", xres, bpp);
  
  // Open the file descriptor.
  int fd = open("/dev/fb0", O_WRONLY | O_NONBLOCK, 0);
  if (fd < 0) {
    perror("Failed to open /dev/dsp");
    return;
  }
  
  // Request the mode.
  struct fb_var_screeninfo info;
  if (ioctl(fd, FBIOGET_VSCREENINFO, &info) < 0) {
    perror("Failed to get screen info");
    close(fd);
    return;
  }
  info.xres = xres;
  info.yres = 0;
  info.bits_per_pixel = bpp;
  if (ioctl(fd, FBIOPUT_VSCREENINFO, &info) < 0) {
    perror("Failed to put screen info");
    close(fd);
    return;
  }
  
  // Print the mode that we actually got.
  fprintf(stderr, "Received mode %dX%d_%dBPP.\n", info.xres, info.yres, info.bits_per_pixel);
  
  // Write a simple colormap.
  unsigned char cmap[256*3];
  unsigned char *cmapp = cmap;
  for (i = 0; i < 64; i++) { *cmapp++ = i << 2; *cmapp++ = 0;      *cmapp++ = 0;      }
  for (i = 0; i < 64; i++) { *cmapp++ = 0;      *cmapp++ = i << 2; *cmapp++ = 0;      }
  for (i = 0; i < 64; i++) { *cmapp++ = 0;      *cmapp++ = 0;      *cmapp++ = i << 2; }
  for (i = 0; i < 64; i++) { *cmapp++ = i << 2; *cmapp++ = i << 2; *cmapp++ = i << 2; }
  if (ioctl(fd, FBIOPUTCMAP, cmap) < 0) {
    perror("Failed to put color map");
    close(fd);
    return;
  }
  
  // Print a few pixels with seeks and short writes to test the scaling
  // algorithm.
  for (i = 0; i < 50; i++) {
    if (wpix(fd, &info, 10,     50+i, i,  0,   0,   0  )) return;
    if (wpix(fd, &info, 20+i,   50+i, 50, 96,  96,  96 )) return;
    if (wpix(fd, &info, 80+i,   50+i, 1,  160, 160, 160)) return;
    if (wpix(fd, &info, -60+i, 150+i, 50, 96,  96,  96 )) return;
  }
  
  // Print color bars.
  for (i = 0; i < 128; i++) {
    if (wpix(fd, &info, 150, 50+i, 40, i<<1, 0, 0)) return;
    if (wpix(fd, &info, 200, 50+i, 40, 0, i<<1, 0)) return;
    if (wpix(fd, &info, 250, 50+i, 40, 0, 0, i<<1)) return;
  }
  
  close(fd);
}

/**
 * Call the MBR to reboot.
 */
void reboot(void) {
  ((void_t)0x40000000)();
}

/**
 * Reads a string from the UART.
 */
char *prompt(void) {
  while (1) {
    fprintf(stderr, "$ ");
    static char buf[256];
    int i;
    for (i = 0; i < 255; i++) {
      while (read(0, buf + i, 1) != 1);
      if (buf[i] == '\n') {
        buf[i] = 0;
        return buf;
      }
    }
    while (1) {
      while (read(0, buf, 1) != 1);
      if (buf[0] == '\n') {
        break;
      }
    }
    fprintf(stderr, "That was way too long! Try again.\n");
  }
}

/**
 * Bootloader main.
 */
int main(void) {
  
  char autorun_buf[129];
  char *autorun = autorun_buf;
  autorun[0] = 0;
  
  // Check for a magic string in CR_SCRP3. This initializes to 0 when the core
  // resets, so this is zero under normal circumstances. The only reason for it
  // to not be zero is if the secondary core reset us and then wrote to the
  // register. It can use this to reboot the application if it detects a crash.
  if (CR_SCRP3) {
    const char *magic = (const char*)CR_SCRP3;
    CR_SCRP3 = 0;
    fprintf(stderr, "Magic reboot string detected at 0x%08X!\n", (long)magic);
    fprintf(stderr, "Value is \"%s\".\n", magic);
    load(magic);
  }
  
  // Check for existence of the autorun file.
  int fd = open("/part1/autorun", O_RDONLY, 0);
  if (fd >= 0) {
    int count = read(fd, autorun, 128);
    if (count < 0) {
      perror("Read from autorun file failed");
    } else if (!count) {
      fprintf(stderr, "Autorun file found, but appears to be empty.\n");
    } else {
      autorun[count] = 0;
      
      // Remove leading space.
      while (*autorun && isspace(*autorun)) autorun++;
      
      // Remove anything after the first \n.
      char *end = strchr(autorun, '\n');
      if (end) *end = 0;
      
      // Remove trailing space.
      end = autorun + strlen(autorun) - 1;
      while ((end >= autorun) && isspace(*end)) *end-- = 0;
      
      fprintf(stderr, "Autorun file contains \"%s\".\n", autorun);
    }
    close(fd);
  } else {
    perror("Opening autorun file failed");
  }
  
  // Handle autorun.
  if (autorun[0]) {
    
    // Autorun timer...
    int fdu = open("/dev/ttyS0", O_RDONLY | O_NONBLOCK, 0);
    if (fdu < 0) perror("open uart");
    
    struct timeval t1;
    gettimeofday(&t1, 0);
    int countdown;
    for (countdown = 6; countdown > 0; countdown--) {
      if (countdown < 6) fprintf(stderr, "Autorun in %d seconds...\n", countdown);
      
      while (1) {
        struct timeval t2;
        gettimeofday(&t2, 0);
        if (t1.tv_sec != t2.tv_sec) {
          t1.tv_sec = t2.tv_sec;
          break;
        }
        char dummy;
        if (read(fdu, &dummy, 1) == 1) {
          close(fdu);
          goto after_autorun;
        }
      }
      
    }
    
    close(fdu);
    
    load(autorun);
  }
  
after_autorun:
  
  // Print welcome message.
  fprintf(stderr, "--------\n"
                  "Welcome to the r-VEX ML605 bootloader!\n");
  const char *help = 
                  "Available commands:\n"
                  " - load <name>     - runs /part1/<name>.*.elf\n"
                  " - ls <dir>        - lists the given directory\n"
                  " - ps2             - tests /dev/input/event*\n"
                  " - audio           - tests /dev/dsp with a 1kHz tri wave\n"
                  " - video [w] [bpp] - switches video mode and shows test pattern\n"
                  " - reboot          - jumps back to the MBR\n"
                  " - help            - this message\n"
                  " - exit            - calls exit(0)\n";
  fprintf(stderr, help);
  
  // Command handler loop.
  while (1) {
    
    // Get input from the user.
    char *cmd = prompt();
    
    // Find the first space.
    char *arg = strchr(cmd, ' ');
    if (arg) *arg++ = 0;
    
    // Handle commands.
    if (!strcmp(cmd, "load"))   { load(arg);             continue; }
    if (!strcmp(cmd, "ls"))     { ls(arg);               continue; }
    if (!strcmp(cmd, "ps2"))    { ps2();                 continue; }
    if (!strcmp(cmd, "audio"))  { audio();               continue; }
    if (!strcmp(cmd, "video"))  { video(arg);            continue; }
    if (!strcmp(cmd, "reboot")) { reboot();              continue; }
    if (!strcmp(cmd, "help"))   { fprintf(stderr, help); continue; }
    if (!strcmp(cmd, "exit"))   { exit(0);               continue; }
    
    // Handle unknown command.
    fprintf(stderr, "Unknown command! Command was \"%s\", argument was \"%s\"\n", cmd, arg);
    
  }
  
}

