
#include <machine/rvex-sys.h>
#include <machine/rvex.h>
#include <rootfs.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <rvudev.h>
#include <sysace.h>
#include <errno.h>
#include <stdio.h>
#include <irqmp.h>
#include <gri2c.h>
#include <grps2.h>
#include <dsp.h>

#include "platform.h"


/******************************************************************************/
/* UART                                                                       */
/******************************************************************************/

/**
 * putsk (puts kernel, sort of) handler. This writes to the UART without making
 * any assumptions about newlib's state. Unlike puts, no trailing newline is
 * appended automatically.
 */
void putsk(const char *msg) {
  while (*msg) {
    while (!(*((volatile char*)0xD1000004) & 0x02));
    *((volatile char*)0xD1000000) = *msg++;
  }
}

/**
 * Configures the UART device files.
 */
static void plat_setup_uart(void) {
  
  // Print the initial hello message without newlib.
  putsk("\nInitializing stdio...\n");
  
  // Create /dev directory for device files.
  if (mkdir("/dev", 0777)) exit(errno);
  
  // Create an r-VEX UART file for stdin/stdout/stderr.
  if (mount_rvudev("/dev/ttyS0", 0xD1000000)) exit(errno);
  
  // Open the UART. This should become fd 0 since no files are open yet,
  // which is stdin.
  if (open("/dev/ttyS0", O_RDWR, 0) != 0) exit(errno);
  
  // dup the file descriptor twice to initialize file descriptors 1 and 2
  // for stdout and stderr.
  if (fcntl(0, F_DUPFD, 1) != 1) exit(errno);
  if (fcntl(0, F_DUPFD, 2) != 2) exit(errno);
  
  // Print boot message.
  fprintf(stderr, "Initialized stdio!\n");
  
}


/******************************************************************************/
/* INTERRUPTS                                                                 */
/******************************************************************************/

// We use libirqmp, so we just have to tell it where the unit is in memory.
IRQMP_AT(0x80000200);


/******************************************************************************/
/* PS/2 DEFINITIONS                                                           */
/******************************************************************************/

// PS/2 peripheral interface.
typedef struct {
  unsigned int data;
  unsigned int status;
  unsigned int control;
  unsigned int timer;
} apbps2_t;

#define PLAT_PS2_ADDR(i) (0x80000000+((i)<<8))
#define PLAT_PS2_IRQ(i)  ((i)+6)
#define PLAT_PS2(i)      ((volatile apbps2_t*)PLAT_PS2_ADDR(i))
#define PLAT_NUM_PS2     2


/******************************************************************************/
/* TIMING                                                                     */
/******************************************************************************/

// GRLIB general purpose timer peripheral. We use the first for storing the
// current microsecond and second for gettimeofday. The second is
// general-purpose.
typedef struct {
  unsigned int scaler_val;
  unsigned int scaler_reload;
  unsigned int config;
  unsigned int latchsel;
  unsigned int tim1_val;
  unsigned int tim1_reload;
  unsigned int tim1_config;
  unsigned int tim1_latch;
  unsigned int tim2_val;
  unsigned int tim2_reload;
  unsigned int tim2_config;
  unsigned int tim2_latch;
  unsigned int tim3_val;
  unsigned int tim3_reload;
  unsigned int tim3_config;
  unsigned int tim3_latch;
  unsigned int tim4_val;
  unsigned int tim4_reload;
  unsigned int tim4_config;
  unsigned int tim4_latch;
} gptimer_t;

#define PLAT_GPTIMER1 ((volatile gptimer_t*)0x80000300)
#define PLAT_GPTIMER2 ((volatile gptimer_t*)0x80000400)

/**
 * Contains the platform operating frequency. Initialized by plat_setup_time.
 */
static int frequency_khz = -1;

/**
 * Initializes timers.
 */
static void plat_setup_time(void) {
  
  fprintf(stderr, "Initializing timers...\n");
  
  // Shamelessly determine the frequency using the PS/2 clock prescaler
  // register, which is set such that the PS/2 clock is 10 kHz.
  frequency_khz = PLAT_PS2(0)->timer * 10;
  printf(" - Detected platform frequency %dkHz.\n", frequency_khz);
  
  // Set the timer1 prescaler such that it rolls over approximately every
  // microsecond for gettimeofday.
  if (frequency_khz % 1000) {
    printf(" - NOTE: frequency in MHz needs to be an integer for gettimeofday accuracy.\n");
  }
  PLAT_GPTIMER1->scaler_reload = (frequency_khz - 500) / 1000;
  PLAT_GPTIMER1->scaler_val = 0;
  
  // Configure the second counter.
  PLAT_GPTIMER1->tim2_val    = 0xFFFFFFFF;
  PLAT_GPTIMER1->tim2_reload = 0xFFFFFFFF;
  PLAT_GPTIMER1->tim2_config = 0x23;
  
  // Configure the microsecond counter.
  PLAT_GPTIMER1->tim1_val    = 999999;
  PLAT_GPTIMER1->tim1_reload = 999999;
  PLAT_GPTIMER1->tim1_config = 0x03;
  
  // Set the time to midnight on January 1st 2018.
  plat_settimeofday(1514764800, 0);
  
  // Have timer2 divide by 2, which is the minimum because there are two timers
  // in the peripheral (this is a thing apparently).
  PLAT_GPTIMER2->scaler_reload = 1;
  PLAT_GPTIMER2->scaler_val = 0;
  
  fprintf(stderr, "Initialized timers!\n");
}

/**
 * Implementation for the gettimeofday syscall. THIS IS MERELY APPROXIMATE if
 * the platform clock frequency in MHz is not an integer.
 */
rvex_sysreturn_t sys_gettimeofday(struct timeval *ptimeval, void *ptimezone) {
  int s1, s2, us;
  
  // Query the timer. The seconds are queried twice to check for microsecond
  // overflow.
  s1 = PLAT_GPTIMER1->tim2_val;
  us = PLAT_GPTIMER1->tim1_val;
  s2 = PLAT_GPTIMER1->tim2_val;
  
  // If the microsecond timer overflowed while checking, assume 0 and use the
  // second query of the seconds. This will necessarily represent a time between
  // the two seconds queries.
  if (s1 != s2) {
    ptimeval->tv_usec = 0;
  } else {
    ptimeval->tv_usec = 999999 - us;
  }
  ptimeval->tv_sec = ~s2;
  
  // Return success.
  rvex_sysreturn_ok(int, 0);
}

/**
 * Sets the current time.
 */
void plat_settimeofday(int sec, int usec) {
  
  // Stop the timers while we do this.
  PLAT_GPTIMER1->tim1_config = 0x02;
  PLAT_GPTIMER1->tim2_config = 0x22;
  
  // Set the timer values.
  PLAT_GPTIMER1->tim1_val = 999999 - usec;
  PLAT_GPTIMER1->tim2_val = ~sec;
  
  // Restart the timers.
  PLAT_GPTIMER1->tim2_config = 0x23;
  PLAT_GPTIMER1->tim1_config = 0x03;
  
}

/**
 * Returns the frequency at which the platform is running in kHz.
 */
int plat_frequency(void) {
  return frequency_khz;
}

/**
 * Delays the specified number of milliseconds.
 */
void plat_delay(int ms) {
  long t = CR_CNT;
  while (ms--) {
    t += frequency_khz;
    while (((int)(t - CR_CNT)) > 0);
  }
}


/******************************************************************************/
/* I2C                                                                        */
/******************************************************************************/

/**
 * Configures the I2C device files.
 */
static void plat_setup_i2c(void) {
  
  fprintf(stderr, "Initializing I2C...\n");
  
  // Mount DVI I2C bus interface.
  if (mount_gri2c("/dev/i2c1", frequency_khz, 0x80000700)) {
    perror(" - Init /dev/i2c1 (DVI) failed");
  }
  
  // Mount power management bus interface.
  if (mount_gri2c("/dev/i2c2", frequency_khz, 0x80000800)) {
    perror(" - Init /dev/i2c2 (pmbus) failed");
  }
  
  // Print completion message.
  fprintf(stderr, "Initialized I2C!\n");
  
}


/******************************************************************************/
/* AUDIO                                                                      */
/******************************************************************************/

#define PLAT_AUDIO_DATA    (*((volatile unsigned char*)0xD2000000))
#define PLAT_AUDIO_REMAIN  (*((const volatile int*)0xD2000000))
#define PLAT_AUDIO_FIFOLEN 4095

/**
 * Configures the audio output file.
 */
static void plat_setup_audio(void) {
  
  fprintf(stderr, "Initializing audio output...\n");
  
  // Configure the /dev/dsp driver.
  mount_dsp_arg_t dsp_cfg = {
    
    // Control flags.
    .flags = DSP_MOUNT_FMT_8BIT
           | DSP_MOUNT_FMT_MONO
           | DSP_MOUNT_FMT_UNSIGNED
           | DSP_MOUNT_FMT_BIG
           | DSP_MOUNT_PRES_DIM_ONE
           | DSP_MOUNT_STAT_REMAIN
           | DSP_MOUNT_SIZE_DIRECT,
    
    // Input frequency to the counter. This is the platform frequency divided
    // by 2 because of the timer prescaler.
    .frequency = frequency_khz * 250,
    
    // Timer scaler registers that control the samplerate.
    .scale1 = &PLAT_GPTIMER2->tim1_reload,
    .scale2 = &PLAT_GPTIMER2->tim1_val,
    
    // Audio peripheral registers.
    .data = (volatile unsigned int*)0xD2000000,
    .status = (volatile unsigned int*)0xD2000000
    
  };
  
  // Size of the audio FIFO. Apparently this can't go into the initializer
  // because this is in an anonymous union? Okay.
  dsp_cfg.size_direct = 4095;
  
  // Mount the /dev/dsp driver.
  if (mount_dsp("/dev/dsp", &dsp_cfg)) {
    perror(" - Init /dev/dsp failed");
  }
  
  // Print completion message.
  fprintf(stderr, "Initialized audio output!\n");
  
}


/******************************************************************************/
/* VIDEO                                                                      */
/******************************************************************************/

/**
 * Register to value mapping for the Chrontel DAC. This sets it up such that
 * VGA and DVI-D both work.
 */
static const unsigned char plat_video_chrontel_init[] = {
  0x1c,  0x04,
  0x1d,  0x45,
  0x1e,  0xf0,
  0x1f,  0x88,
  0x20,  0x22,
  0x21,  0x09,
  0x23,  0x00,
  0x31,  0x80,
  0x33,  0x08,
  0x34,  0x16,
  0x35,  0x30,
  0x36,  0x60,
  0x37,  0x00,
  0x48,  0x18,
  0x49,  0xc0,
  0x4a,  0x95,
  0x4b,  0x17,
  0x56,  0x00,
  0
};

/**
 * Configures the video output.
 */
void plat_setup_video(void) {
  
  fprintf(stderr, "Initializing video...\n");
  
  // Open the I2C device file that the Chrontel DAC is connected to.
  int fd = open("/dev/i2c1", O_RDWR, 0);
  if (fd < 0) {
    perror(" - I2C peripheral open");
  } else {
    
    // Select the Chrontel DAC I2C address.
    if (ioctl(fd, I2C_SLAVE, (void*)0x76) < 0) {
      perror(" - I2C peripheral address select");
    } else {
      
      // Write the registers.
      const unsigned char *ptr = plat_video_chrontel_init;
      while (*ptr) {
        if (lseek(fd, ptr[0], SEEK_SET) < 0) {
          perror(" - Chrontel seek");
          break;
        }
        if (write(fd, ptr+1, 1) != 1) {
          perror(" - Chrontel write");
          break;
        }
        ptr += 2;
      }
      
    }
    
    // Close device file.
    close(fd);
  }
  
  // Mount the /dev/fb0 driver.
  if (mount_grsvga("/dev/fb0", 0x80000600)) {
    perror(" - Init /dev/fb0 failed");
  }
  
  fprintf(stderr, "Initialized video!\n");
  
}


/******************************************************************************/
/* PS/2                                                                       */
/******************************************************************************/

/**
 * Configures the PS/2 device files.
 */
static void plat_setup_ps2(void) {
  
  fprintf(stderr, "Initializing PS/2...\n");
  
  // Create the /dev/input directory.
  if (mkdir("/dev/input", 0777)) {
    perror("Init failed: mkdir /dev/input");
    return;
  }
  
  // Mount interfaces one by one.
  int i;
  for (i = 0; i < PLAT_NUM_PS2; i++) {
    char fname[24];
    snprintf(fname, 24, "/dev/input/event%d", i+1);
    if (mount_grps2(fname, PLAT_PS2_ADDR(i), PLAT_PS2_IRQ(i))) {
      fprintf(stderr, " - Failed port %d: %s\n", i+1, strerror(errno));
    } else {
      fprintf(stderr, " - Connected port %d\n", i+1);
    }
  }
  
  // Print completion message.
  fprintf(stderr, "Initialized PS/2!\n");
  
}


/******************************************************************************/
/* SYSACE                                                                     */
/******************************************************************************/

/**
 * Configures the system ACE (compactflash) device files and FAT mounts.
 */
static void plat_setup_sysace(void) {
  
  fprintf(stderr, "Initializing sysace...\n");
  
  // Mount the sysace.
  if (mount_sysace("/dev/sda", 0x40000000)) {
    perror("mount sysace");
    exit(errno);
  }
  if (mount_mbr("/dev/sda")) {
    perror("mount /dev/sda partitions");
    exit(errno);
  }
  
  // Mount the partitions we found. The first partition is mounted read-only.
  char partbuf[5];
  partbuf[4] = 0;
  int i;
  for (i = 1; i <= 4; i++) {
    partbuf[i-1] = '-';
    
    // Mount the first partition as read-only.
    int ro = i == 1;
    
    // Figure out the filenames.
    char devname[32];
    char mntname[32];
    snprintf(devname, 32, "/dev/sda%d", i);
    snprintf(mntname, 32, "/part%d", i);
    
    // Check whether the device file exists.
    struct stat dummy;
    if (stat(devname, &dummy) < 0) {
      fprintf(stderr, " - stat %s failed: %s\n", devname, strerror(errno));
      continue;
    }
    
    // Create the mount point directory.
    if (mkdir(mntname, 0777)) exit(errno);
    
    // Try to mount the filesystem.
    if (mount_fatfs(devname, mntname, ro)) {
      
      // Mount failed.
      fprintf(stderr, " - Mounting %s failed: %s\n", devname, strerror(errno));
      
      // Try to remove the mount point directory.
      unlink(mntname);
      
    } else {
      
      // Mount OK.
      fprintf(stderr, " - Mounted %s to %s%s\n", devname, mntname, ro ? " (read only)" : "");
      
    }
    
  }
  
  // Print completion message.
  fprintf(stderr, "Initialized sysace!\n", partbuf);
  
}


/******************************************************************************/
/* SETUP/SHUTDOWN HOOKS                                                       */
/******************************************************************************/

/**
 * Platform initialization routine, called before main().
 */
static void plat_setup(void) {
  plat_setup_uart();
  plat_setup_time();
  plat_setup_i2c();
  plat_setup_audio();
  plat_setup_video();
  plat_setup_ps2();
  plat_setup_sysace();
  
  // Enable interrupts when we return from setup.
  CR_SCRP2 = (CR_SCRP2 & ~CR_CCR_IEN_C) | CR_CCR_IEN;
  
  // Print that we're done initializing.
  fprintf(stderr, "Initialization complete, passing control to bootloader main().\n--------\n");
  
}

// Register the setup function.
PNP_REGISTER_SETUP(plat_setup, plat_setup);
