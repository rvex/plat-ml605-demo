
#include "elf.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

/**
 * ELF header structure.
 */
typedef struct elf_ehdr_t {
  unsigned char  e_ident[16];
  unsigned short e_type;
  unsigned short e_machine;
  unsigned int   e_version;
  unsigned long  e_entry;
  unsigned long  e_phoff;
  unsigned long  e_shoff;
  unsigned int   e_flags;
  unsigned short e_ehsize;
  unsigned short e_phentsize;
  unsigned short e_phnum;
  unsigned short e_shentsize;
  unsigned short e_shnum;
  unsigned short e_shstrndx;
} elf_ehdr_t;

/**
 * Program header entry structure.
 */
typedef struct elf_phdr_t {
  unsigned long  p_type;
  unsigned long  p_offset;
  unsigned long  p_vaddr;
  unsigned long  p_paddr;
  unsigned long  p_filesz;
  unsigned long  p_memsz;
  unsigned int   p_flags;
  unsigned long  p_align;
} elf_phdr_t;

/**
 * Section header entry structure.
 */
typedef struct elf_shdr_t {
  unsigned int   sh_name;
  unsigned int   sh_type;
  unsigned long  sh_flags;
  unsigned long  sh_addr;
  unsigned long  sh_offset;
  unsigned long  sh_size;
  unsigned int   sh_link;
  unsigned int   sh_info;
  unsigned int   sh_addralign;
  unsigned long  sh_entsize;
} elf_shdr_t;

/**
 * sh_type values.
 */
#define SHT_NULL                0x00000000
#define SHT_PROGBITS            0x00000001
#define SHT_SYMTAB              0x00000002
#define SHT_STRTAB              0x00000003
#define SHT_RELA                0x00000004
#define SHT_HASH                0x00000005
#define SHT_DYNAMIC             0x00000006
#define SHT_NOTE                0x00000007
#define SHT_NOBITS              0x00000008
#define SHT_REL                 0x00000009
#define SHT_SHLIB               0x0000000A
#define SHT_DYNSYM              0x0000000B
#define SHT_INIT_ARRAY          0x0000000E
#define SHT_FINI_ARRAY          0x0000000F
#define SHT_PREINIT_ARRAY       0x00000010
#define SHT_GROUP               0x00000011
#define SHT_SYMTAB_SHNDX        0x00000012
#define SHT_NUM                 0x00000013

/**
 * sh_flags bits.
 */
#define SHF_WRITE               0x00000001
#define SHF_ALLOC               0x00000002
#define SHF_EXECINSTR           0x00000004
#define SHF_MERGE               0x00000010
#define SHF_STRINGS             0x00000020
#define SHF_INFO_LINK           0x00000040
#define SHF_LINK_ORDER          0x00000080
#define SHF_OS_NONCONFORMING    0x00000100
#define SHF_GROUP               0x00000200
#define SHF_TLS                 0x00000400

/**
 * Symbol table entry.
 */
typedef struct elf_sym_t {
  unsigned int    st_name;
  unsigned long   st_value;
  unsigned int    st_size;
  unsigned char   st_info;
  unsigned char   st_other;
  unsigned short  st_shndx;
} elf_sym_t;

/**
 * Symbol info macros.
 */
#define ST_BIND(i)              ((i) >> 4)
#define ST_TYPE(i)              ((i) & 0xf)
#define ST_INFO(b, t)           (((b)<<4)+((t)&0xf))

/**
 * Symbol bind constants.
 */
#define STB_LOCAL               0
#define STB_GLOBAL              1
#define STB_WEAK                2

/**
 * Symbol type constants.
 */
#define STT_NOTYPE              0
#define STT_OBJECT              1
#define STT_FUNC                2
#define STT_SECTION             3
#define STT_FILE                4

/**
 * Parses the ELF file specified by fname.
 * 
 * If offs is non-null, the ALLOC sections in the ELF file are loaded into
 * memory at the specified offsets. If ill is also specified, the sections are
 * first checked for intersection with the regions specified therein; if any
 * intersection is found the ELF is rejected before any data is loaded. ill
 * should be an array terminated by a record with start and end both zero.
 * start is inclusive, end is non-inclusive.
 * 
 * If sym is non-null, the symbol table is read to look for the symbols listed
 * in the sym list. The name fields should be initialized to the section names
 * by the caller. addr should be initialized to 0xFFFFFFFF by the caller. addr
 * is updated by elf_load when the associated symbol is found and has an
 * address lower than the current one. Thus, if multiple symbols with the same
 * name exist, the lowest address is returned.
 * 
 * The return value is 0 for success and -1 for failure.
 */
int elf_load(
  const char *fname,
  const elf_mem_offs_t *mem_offs,
  const elf_mem_ill_t *mem_ill,
  elf_sym_req_t *sym
) {
  int i;
  unsigned long offs;
  elf_ehdr_t ehdr;
  elf_shdr_t shent;
  elf_shdr_t shent_symtab;
  elf_shdr_t shent_strtab;
  char *sect_strings = NULL;
  int error = 0;
  
  fprintf(stderr, "Parsing ELF file \"%s\"...\n", fname);
  
  // Open the file.
  int fd = open(fname, O_RDONLY, 0);
  if (fd < 0) { perror("Open failed"); error = -1; goto end; }
  
  // Read the ELF header.
  if (read(fd, &ehdr, sizeof(elf_ehdr_t)) != sizeof(elf_ehdr_t)) { perror("Read ELF header failed"); error = -1; goto end; }
  
  // Check the ELF header.
  static const char e_ident_expected[] = {0x7F, 'E', 'L', 'F', 1, 2, 1};
  if (memcmp(ehdr.e_ident, e_ident_expected, sizeof(e_ident_expected))) { fprintf(stderr, "Magic number incorrect\n"); error = -1; goto end; }
  
  // Load the string table.
  offs = ehdr.e_shoff + ehdr.e_shentsize * ehdr.e_shstrndx;
  if (lseek(fd, offs, SEEK_SET) < 0) { perror("Seek to section string table header failed"); error = -1; goto end; }
  if (read(fd, &shent, sizeof(elf_shdr_t)) != sizeof(elf_shdr_t)) { perror("Read section string table header failed"); error = -1; goto end; }
  
  // Allocate a buffer for the string table.
  unsigned long strings_size = shent.sh_size;
  sect_strings = (char*)malloc(strings_size + 1);
  if (!sect_strings) { perror("Allocating buffer for section string table failed"); error = -1; goto end; }
  sect_strings[strings_size] = 0;
  if (lseek(fd, shent.sh_offset, SEEK_SET) < 0) { perror("Seek to section string table failed"); error = -1; goto end; }
  if (read(fd, sect_strings, strings_size) != strings_size) { perror("Read section string table failed"); error = -1; goto end; }
  
  // Iterate through the sections to check validity and compatibility of the
  // ELF file.
  shent_symtab.sh_type = SHT_NULL;
  shent_strtab.sh_type = SHT_NULL;
  for (i = 0; i < ehdr.e_shnum; i++) {
    
    // Read the section header entry.
    offs = ehdr.e_shoff + ehdr.e_shentsize * i;
    if (lseek(fd, offs, SEEK_SET) < 0) { perror("Seek to section header entry failed"); error = -1; goto end; }
    if (read(fd, &shent, sizeof(elf_shdr_t)) != sizeof(elf_shdr_t)) { perror("Read section header entry failed"); error = -1; goto end; }
    
    // Get the name of the section.
    if (shent.sh_name >= strings_size) { fprintf(stderr, "Section name string index out of range\n"); error = -1; goto end; }
    const char *name = sect_strings + shent.sh_name;
    
    // If memory offsets are specified and this section has the ALLOC flag,
    // check that the section does not intersect with the illegal regions.
    if (mem_offs && mem_ill && shent.sh_flags & SHF_ALLOC) {
      
      // Figure out whether this section is data or instruction.
      int insn = shent.sh_flags & SHF_EXECINSTR;
      unsigned long start = insn ? mem_offs->imem : mem_offs->dmem;
      start += shent.sh_addr;
      unsigned long end = start + shent.sh_size;
      
      // Check that the target area doesn't intersect with the bootloader.
      const elf_mem_ill_t *ill = mem_ill;
      while (ill->start || ill->end) {
        if ((end > ill->start) && (ill->end > start)) {
          fprintf(stderr, "ELF section 0x%08X..0x%08X intersects with illegal range 0x%08X..0x%08X\n",
            start, end, ill->start || ill->end);
          error = -1; goto end;
        }
        ill++;
      }
      
    }
    
    // If symbol queries are specified, store the symbol and string table
    // headers.
    if (sym) {
      if ((shent.sh_type == SHT_SYMTAB) && !strcmp(name, ".symtab")) {
        shent_symtab = shent;
      } else if ((shent.sh_type == SHT_STRTAB) && !strcmp(name, ".strtab")) {
        shent_strtab = shent;
      }
    }
    
  }
  
  // Look up the symbol queries, if any are specified.
  if (sym && shent_symtab.sh_type && shent_strtab.sh_type) {
    
    // Figure out how many symbol entries there are.
    int syms_remain = shent_symtab.sh_size / sizeof(elf_sym_t);
    int syms_read = 0;
    
    while (syms_remain) {
      
      // Read the next (up to) 16 symbols.
      elf_sym_t symbuf[16];
      int count = syms_remain;
      if (count > 16) count = 16;
      
      if (lseek(fd, shent_symtab.sh_offset + syms_read * sizeof(elf_sym_t), SEEK_SET) < 0) { perror("Seek to symbol entry failed"); error = -1; goto end; }
      count = read(fd, symbuf, count * sizeof(elf_sym_t));
      if (count < 0) { perror("Read symbol entries failed"); error = -1; goto end; }
      
      // Parse the symbols that we just read.
      elf_sym_t *symptr = symbuf;
      while (count >= sizeof(elf_sym_t)) {
        
        // Handle only object and function symbols with a name.
        if (symptr->st_name && (ST_TYPE(symptr->st_info) == STT_OBJECT || ST_TYPE(symptr->st_info) == STT_FUNC)) {
          
          // Look up this symbol's name.
          if (lseek(fd, shent_strtab.sh_offset + symptr->st_name, SEEK_SET) < 0) { perror("Seek to symbol name failed"); error = -1; goto end; }
          char nambuf[256];
          int namcount = read(fd, nambuf, 255);
          if (namcount < 0) { perror("Read symbol name failed"); error = -1; goto end; }
          nambuf[namcount] = 0;
          
          // Look through the symbol request list for a match.
          elf_sym_req_t *sym_req = sym;
          while (sym_req->name) {
            if (!strcmp(sym_req->name, nambuf)) {
              
              // Match!
              if (symptr->st_value < sym_req->addr) {
                sym_req->addr = symptr->st_value;
              }
              
            }
            
            sym_req++;
          }
          
        }
        
        count -= sizeof(elf_sym_t);
        symptr++;
        syms_remain--;
        syms_read++;
      }
      
    }
    
  }
  
  // Load the ALLOC sections into memory if memory offsets are specified.
  if (mem_offs) {
    
    fprintf(stderr, "Loading ELF file \"%s\" into memory...\n", fname);
    
    for (i = 0; i < ehdr.e_shnum; i++) {
      
      // Read the section header entry.
      offs = ehdr.e_shoff + ehdr.e_shentsize * i;
      if (lseek(fd, offs, SEEK_SET) < 0) { perror("Seek to section header entry failed"); error = -1; goto end; }
      if (read(fd, &shent, sizeof(elf_shdr_t)) != sizeof(elf_shdr_t)) { perror("Read section header entry failed"); error = -1; goto end; }
      
      // Get the name of the section.
      if (shent.sh_name >= strings_size) { fprintf(stderr, "Section name string index out of range\n"); error = -1; goto end; }
      const char *name = sect_strings + shent.sh_name;
      
      // If this section has the ALLOC flag, load it into memory.
      if (shent.sh_flags & SHF_ALLOC) {
        
        // Figure out whether this section is data or instruction.
        int insn = shent.sh_flags & SHF_EXECINSTR;
        unsigned long target = insn ? mem_offs->imem : mem_offs->dmem;
        target += shent.sh_addr;
        
        // Print what we're about to do.
        if (shent.sh_type == SHT_PROGBITS) {
          
          // Load into memory.
          fprintf(stderr,
                  "Loading section %s (%lu bytes) to address 0x%08lX (%cmem)\n",
                  name, shent.sh_size, target, insn ? 'i' : 'd');
          if (lseek(fd, shent.sh_offset, SEEK_SET) < 0) { perror("Seek to section contents failed"); error = -1; goto end; }
          unsigned long remain = shent.sh_size;
          
          // Read will fail with EINVAL if the buffer address is zero, because
          // it thinks it's NULL. So in that case we need to read the first
          // byte to a temporary buffer and then copy it ourselves.
          if (remain && !target) {
            char x;
            off_t count = read(fd, &x, 1);
            if (count <= 0) { perror("Read section contents failed"); error = -1; goto end; }
            *((volatile char*)0) = x;
            remain -= 1;
            target += 1;
          }
          
          while (remain) {
            off_t count = read(fd, (void*)target, remain);
            if (count <= 0) { perror("Read section contents failed"); error = -1; goto end; }
            remain -= count;
            target += count;
          }
          
        } else if (shent.sh_type == SHT_NOBITS) {
          
          // Clear memory.
          fprintf(stderr,
                  "Clearing section %s (%lu bytes) at address 0x%08lX (%cmem)\n",
                  name, shent.sh_size, target, insn ? 'i' : 'd');
          memset((void*)target, 0, shent.sh_size);
          
        }
        
      }
      
    }
    
    fprintf(stderr, "Successfully loaded ELF file!\n");
    
  }
  
end:
  free(sect_strings);
  if (fd >= 0) close(fd);
  return error;
}

