
#ifndef _ELF_H_
#define _ELF_H_

/**
 * Defines memory load offsets.
 */
typedef struct elf_mem_offs_t {
  unsigned long imem;
  unsigned long dmem;
} elf_mem_offs_t;

/**
 * Defines a memory range that elf_load isn't allowed to touch.
 */
typedef struct elf_mem_ill_t {
  unsigned long start;
  unsigned long end;
} elf_mem_ill_t;

/**
 * Symbol query. name should be specified by the caller, and addr should be
 * initialized to 0xFFFFFFFF (or -1). elf_load will write addr to the lowest
 * found symbol address.
 */
typedef struct elf_sym_req_t {
  const char *name;
  unsigned long addr;
} elf_sym_req_t;

/**
 * Parses the ELF file specified by fname.
 * 
 * If offs is non-null, the ALLOC sections in the ELF file are loaded into
 * memory at the specified offsets. If ill is also specified, the sections are
 * first checked for intersection with the regions specified therein; if any
 * intersection is found the ELF is rejected before any data is loaded. ill
 * should be an array terminated by a record with start and end both zero.
 * start is inclusive, end is non-inclusive.
 * 
 * If sym is non-null, the symbol table is read to look for the symbols listed
 * in the sym list. The name fields should be initialized to the section names
 * by the caller. addr should be initialized to 0xFFFFFFFF by the caller. addr
 * is updated by elf_load when the associated symbol is found and has an
 * address lower than the current one. Thus, if multiple symbols with the same
 * name exist, the lowest address is returned.
 */
int elf_load(
  const char *fname,
  const elf_mem_offs_t *mem_offs,
  const elf_mem_ill_t *mem_ill,
  elf_sym_req_t *sym
);

#endif
